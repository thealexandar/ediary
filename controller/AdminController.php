<?php

class AdminController extends Controller {

  public function __construct() {
    $this->userModel = $this->model('UserModel');
    $this->subjectModel = $this->model('SubjectModel');
    $this->classesModel = $this->model('ClassesModel');
    $this->teacherModel = $this->model('TeacherModel');
    $this->teacherSubjectClassModel = $this->model('TeacherSubjectClassModel');
    $this->studentModel = $this->model('StudentModel');
    $this->timetableModel = $this->model('TimetableModel');
    $this->noticeboardModel = $this->model('NoticeboardModel');
    $this->daysModel = $this->model('DaysModel');
    $this->lessonsModel = $this->model('LessonsModel');

    if(!isset($_SESSION['user_id'])) {
      header("Location: ../Users/login");
    }
  }

  function index() {
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/dashboard_admin");
    $this->loadView("footer");
  }

  function dashboard_admin() {
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/dashboard_admin");
    $this->loadView("footer");
  }

  function users_admin() {
    $tdelete = $this->userModel->tdelete(); 
    $adelete = $this->userModel->adelete(); 
    $hdelete = $this->userModel->hdelete(); 
    $users = $this->userModel->getAll();
    $add = $this->userModel->add();
    $update = $this->userModel->update('id');
    $data = [
      'user' => $users,
      'update' => $update
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/users_admin", $data);
    $this->loadView("footer");
  }
  
    public function subjects_admin() {
    $delete = $this->subjectModel->delete(); 
    $subjects = $this->subjectModel->getAll();
    $add = $this->subjectModel->add();
    $update = $this->subjectModel->update();
    $data = [
      'subj' => $subjects
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/subjects_admin", $data); 
    $this->loadView("footer");
  }
  
  function classes_admin() {
    $add = $this->classesModel->add();
    $filter = $this->classesModel->filter();
    $delete = $this->classesModel->delete();
    $data = [
      'class' => $filter
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/classes_admin", $data);
    $this->loadView("footer");
  }

  function class_details() {
    $delete = $this->studentModel->delete(); 
    $classes = $this->studentModel->getByClass($_GET["class"]);
    $add = $this->studentModel->add($_GET["class"]);
    $update = $this->studentModel->update("id");
    $data = [
      'class' => $classes
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/class_details", $data);
    $this->loadView("footer");
  }

  function timetable_admin() {
    $filter = $this->classesModel->filter();
    $data = [
      'filter' => $filter
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/timetable_admin", $data);
    $this->loadView("footer");
  }

  function timetable_add() {
    $delete = $this->timetableModel->delete(); 
    $add = $this->timetableModel->add($_GET["class"]);
    $update = $this->timetableModel->update();
    $subj = $this->subjectModel->getSubjectsByClass($_GET["class"]);
    $lesson = $this->lessonsModel->getAll();
    $day = $this->daysModel->getAll();
    $get = $this->timetableModel->getForAdmin($_GET["class"]);
    $mon = $this->timetableModel->getForAdmin($_GET["class"]."  AND d.id_days=1");
    $tue = $this->timetableModel->getForAdmin($_GET["class"]."  AND d.id_days=2");
    $wed = $this->timetableModel->getForAdmin($_GET["class"]."  AND d.id_days=3");
    $thu = $this->timetableModel->getForAdmin($_GET["class"] ." AND d.id_days=4");
    $fri = $this->timetableModel->getForAdmin($_GET["class"] ." AND d.id_days=5");
    $data = [
      'timetable' => $get,
      'subj' => $subj,
      'lesson' =>$lesson,
      'day' =>$day,
      'mon' => $mon,
      'tue' => $tue,
      'wed' => $wed,
      'thu' => $thu,
      'fri' => $fri
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/timetable_add", $data);
    $this->loadView("footer");
  }

  function noticeboard_admin() {
    $delete = $this->noticeboardModel->delete();
    $get = $this->noticeboardModel->getAll();
    $add = $this->noticeboardModel->add();
    $data = [
      'notice' => $get
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/noticeboard_admin", $data);
    $this->loadView("footer");
  }

  function noticeboard_details() {
    $get = $this->noticeboardModel->getById($_GET['id']);
    $data = [
      'notice' => $get
    ];
     $this->loadView("header");
     $this->loadView("sidebar");
     $this->loadView("Admin/noticeboard_details", $data);
     $this->loadView("footer");
   }

  function teachers_admin() {
    $teachers = $this->teacherModel->all();
    $data = [
      'teach' => $teachers
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/teachers_admin", $data);
    $this->loadView("footer");
  }

  function teacher_classes() {
    $delete = $this->teacherSubjectClassModel->deleteClass(); 
    $get = $this->teacherSubjectClassModel->getClass($_GET["teacher"]);
    $add = $this->teacherSubjectClassModel->addClass($_GET["teacher"]);
    $class = $this->classesModel->getAll();
    $data = [
      'get' => $get,
      'class' => $class
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/teacher_classes", $data);
    $this->loadView("footer");
  }

  function teacher_subjects() {
    $delete = $this->teacherSubjectClassModel->deleteSubject(); 
    $get = $this->teacherSubjectClassModel->getSubjects($_GET["teacher"]);
    $add = $this->teacherSubjectClassModel->addSubject($_GET["teacher"]);
    $subject = $this->subjectModel->getAll();
    $data = [
      'get' => $get,
      'subject' => $subject
    ];
    $this->loadView("header");
    $this->loadView("sidebar");
    $this->loadView("Admin/teacher_subjects", $data);
    $this->loadView("footer");
  }
}