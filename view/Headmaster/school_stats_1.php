<main role="main" class="ml-sm-auto px-4 main">
  <div class="pt-3 pb-2 mb-3 border-bottom text-center">
      <h1 class="h2">Overall Statistics By Subjects</h1>
    </div>


    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
   
   <div class="input-group-prepend d-block col-12 pl-0 pr-0">
       <a href="Headmaster/school_statistics" class="btn btn-info float-left" role="button">Back</a>
   </div>
 </div>
 

 <script>
            var chart;

            var chartData = [
                    <?php 
                    $grades = $data['grades'];
                    foreach($grades as $grade) { ?>
                    {
                    "subject": "<?= $grade->subject; ?>",
                    "average1": "<?= $grade->average1; ?>",
                    "average2": "<?= $grade->average2; ?>"
                    },
                    <?php } ?>
            ];


            AmCharts.ready(function () {
                // SERIAL CHART
                chart = new AmCharts.AmSerialChart();
                chart.dataProvider = chartData;
                chart.categoryField = "subject";
                chart.color = "#FFFFFF";
                chart.fontSize = 14;
                chart.startDuration = 1;
                chart.plotAreaFillAlphas = 0.2;
                // the following two lines makes chart 3D
                chart.angle = 30;
                chart.depth3D = 60;

                // AXES
                // category
                var categoryAxis = chart.categoryAxis;
                categoryAxis.gridAlpha = 0.2;
                categoryAxis.gridPosition = "start";
                categoryAxis.gridColor = "#FFFFFF";
                categoryAxis.axisColor = "#FFFFFF";
                categoryAxis.axisAlpha = 0.5;
                categoryAxis.dashLength = 5;

                // value
                var valueAxis = new AmCharts.ValueAxis();
                valueAxis.stackType = "3d"; // This line makes chart 3D stacked (columns are placed one behind another)
                valueAxis.gridAlpha = 0.2;
                valueAxis.gridColor = "#FFFFFF";
                valueAxis.axisColor = "#FFFFFF";
                valueAxis.axisAlpha = 0.5;
                valueAxis.dashLength = 5;
                valueAxis.title = "Average Grades By Subjects";
                valueAxis.titleColor = "#FFFFFF";
                valueAxis.unit = "";
                chart.addValueAxis(valueAxis);

                // GRAPHS
                // first graph
                var graph1 = new AmCharts.AmGraph();
                graph1.title = "Final Average Grades";
                graph1.valueField = "average2";
                graph1.type = "column";
                graph1.lineAlpha = 0;
                graph1.lineColor = "#5cb85c";
                graph1.fillAlphas = 1;
                graph1.balloonText = "Half-Year Average Grade in [[subject]]: <b>[[average2]]</b>";
                chart.addGraph(graph1);

                // second graph
                var graph2 = new AmCharts.AmGraph();
                graph2.title = "Half-Year Average Grades";
                graph2.valueField = "average1";
                graph2.type = "column";
                graph2.lineAlpha = 0;
                graph2.lineColor = "#5bc0de";
                graph2.fillAlphas = 1;
                graph2.balloonText = "Final Average Grade in [[subject]]: <b>[[average1]]</b>";
                chart.addGraph(graph2);

                chart.write("chartdiv");
            });
        </script>
   

    <body style="background-color:#000000;">
        <div id="chartdiv" style="width: 100%; height: 400px;"></div>
    </body>




