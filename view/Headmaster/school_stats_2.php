<main role="main" class="ml-sm-auto px-4 main">
  <div class="pt-3 pb-2 mb-3 border-bottom text-center">
      <h1 class="h2">Overall Statistics By Subjects</h1>
    </div>


    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
   
   <div class="input-group-prepend d-block col-12 pl-0 pr-0">
       <a href="Headmaster/school_statistics" class="btn btn-info float-left" role="button">Back</a>
   </div>
 </div>
 
 <div class="row">
 <script>
            var chart;
            var legend;

            var chartData = [
            <?php 
                $grades = $data['grades'];
                foreach($grades as $grade) { ?>
            {
                "subject": "<?= $grade->subject; ?>",
                "grade": "<?= $grade->average; ?>"
            },
            <?php } ?>
            ];

            
            AmCharts.ready(function () {
                // PIE CHART
                chart = new AmCharts.AmPieChart();
                chart.dataProvider = chartData;
                chart.titleField = "subject";
                chart.valueField = "grade";
                chart.outlineColor = "#FFFFFF";
                chart.outlineAlpha = 0.8;
                chart.outlineThickness = 3;
                chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>";
                // this makes the chart 3D
                chart.depth3D = 20;
                chart.angle = 30;

                // WRITE
                chart.write("chartdiv");
            });
        </script>
    </head>

    <body>
        <div id="chartdiv" style="width: 100%; height: 600px;"></div>
    </body>


 