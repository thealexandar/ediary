<main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
      <h1 class="h2">Your Open Door Parent Requests</h1>
    </div>

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
   
      <div class="input-group-prepend d-block col-12 pl-0 pr-0">
          <a href="Teacher/teachers_opendoor" class="btn btn-info float-left" role="button">Back to Opendoor Section</a>
      </div>
    </div>

  <?php 
  $responses = $data['opendoor'];
  foreach($responses as $response) { ?>
    <div class="col-12 pt-5">
    <div class="card text-center">
    <div class="card-header text-white">
      Open Door Meeting
    </div>
    <div class="card-body ">
      <h4 class="card-title mb-2">Scheduled for <?= $response->day . " at ". $response->hour . " : " . $response->min." ".$response->period; ?></h4>
      <p class="card-text"> Student: <?=$response->name . " " . $response->surname;?></p>
      <p class="card-text">Parent: <?=$response->pname . " " . $response->psurname; ?></p>
      <p class="card-text">Sent: <?=$response->timestamp; ?></p>

      <form action="" method="post" >
        <input type="hidden" name="id_o" value="<?= $response->id_o;?>">
        <button name="accept" type="submit" class="btn btn-primary" onclick="return accepted();">Accept</button>
        <input type="hidden" name="id_o" value="<?= $response->id_o;?>">
        <button name="decline" type="submit" class="btn btn-info" onclick="return declined();">Decline</button>
      </form>

    </div>
    </div>
  </div>       
  <?php } ?>
</main>



