<div class=" text-center sidebar">
        <div class="avatar ml-auto mr-auto mb-3">
            <i class="material-icons avatar-icon">
                person
            </i>
        </div>
    <?php if(isset($_SESSION['user_id'])) : ?>
        <h1 class="text-light ml-auto mb-0"><?php echo $_SESSION['user_username']; ?></h1>
        <ul class="nav flex-column mb-5">
            <li class="nav-item logout">
            <a class="nav-link pl-0 pr-0" href="Users/logout">Sign out</a>
            </li>
        </ul>
    <?php endif; ?>
    <hr class="sidebar-hr">

    <ul class="nav flex-column text-left ml-auto mr-auto sidebar-nav">
        <li class="nav-item">
            <a class="nav-link active" href="Admin/dashboard_admin">
            <span><i class="fas fa-chart-line sidebar-icon"></i></span>
            Dashboard <span class="sr-only">(current)</span>
            </a>
        </li>
        <li class="nav-item hvr-underline-from-center">
            <a class="nav-link" href="Admin/users_admin">
            <span><i class="material-icons sidebar-icon">group</i></span>
            Users
            </a>
        </li>
            <li class="nav-item">
            <a class="nav-link" href="Admin/subjects_admin">
                <span><i class="material-icons sidebar-icon">school</i></span>
            Subjects
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="Admin/teachers_admin">
                <span><i class="material-icons sidebar-icon">person</i></span>
            Teachers
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="Admin/classes_admin">
                <span><i class="material-icons sidebar-icon">format_list_bulleted</i></span>
            Students-Classes
            </a>
        </li>
        <li class="nav-item">

            <a class="nav-link" href="Admin/timetable_admin">

                <span><i class="material-icons sidebar-icon">schedule</i></span>
            Timetable
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="Admin/noticeboard_admin">
                <span><i class="material-icons sidebar-icon">info</i></span>
            Noticeboard
            </a>
        </li>
    </ul>
    </div>
