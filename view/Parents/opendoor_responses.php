<main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
    <h1 class="h2">Your Open Door Teacher Responses</h1>
  </div>
   <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
   
   <div class="input-group-prepend d-block col-12 pl-0 pr-0">
       <a href="Parents/opendoor_parent" class="btn btn-info float-left" role="button">Back to Opendoor Section</a>
   </div>
 </div>

  <?php 
  $responses = $data['opendoor'];
  foreach($responses as $response) { 
    if($response->status==1) { ?>
    <div class="col-12 pt-5">
    <div class="card text-center">
    <div class="card-header text-white">
      Open Door Meeting Response
    </div>
    <div class="card-body ">
      <h4 class="card-title mb-2">Requested for: <?= $response->day . " at ". $response->hour . " : " . $response->min." ".$response->period; ?></h4>
      <p class="card-text"> Teacher: <?=$response->tname . " " . $response->tsurname;?> has accepted your request.</p>
      <p class="card-text">Sent: <?=$response->timestamp; ?></p>
    </div>
    </div>  
  </div>     
  <?php 
  }else{?>
    <div class="col-12 pt-5">
    <div class="card text-center">
    <div class="card-header text-white">
      Open Door Meeting Response
    </div>
    <div class="card-body ">
      <h4 class="card-title mb-2">Requested for: <?= $response->day . " at ". $response->hour . " : " . $response->min." ".$response->period; ?></h4>
      <p class="card-text"> Teacher: <?=$response->tname . " " . $response->tsurname;?> has declined your request. Try choosing another timeslot.</p>
      <p class="card-text">Sent at: <?=$response->timestamp; ?></p>
    </div>
    </div> 
    </div>      
  <?php } } ?>
</main>
