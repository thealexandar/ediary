<?php
  
class HoursModel {

  private $conn;

  public function __construct() {
    $this->conn = new Database();
    }
    
  public function getAll(){
    $this->conn->query("SELECT * FROM hours");
      $results = $this->conn->resultset();
      return $results;
    } 
}