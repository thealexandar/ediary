<?php


class TeacherModel {


	private $conn;

	public function __construct() {
		$this->conn = new Database();
	}

	public function all() {
		$this->conn->query("SELECT * FROM teachers");
		$results = $this->conn->resultset();
		if($results) {
			return $results;
		}else {
			return false;
		}
	}


	public function getById($filter="") {
		$this->conn->query("SELECT 
							*
							FROM 
							teachers t
							JOIN teacher_class tc ON t.id = tc.id_teacher
							JOIN classes ON tc.id_class = classes.id_class
							JOIN teacher_subject ts ON t.id = ts.id_teacher
							JOIN subjects s ON ts.id_subject = s.id
							WHERE " . $filter
							);
		$results = $this->conn->resultset();
		return $results;
	}

	public function delete() {
		if(isset($_POST['delete'])) {
			$id = $_POST['id'];
			$this->conn ->query("delete from teachers where id = {$id}");
			$results = $this->conn->execute();
			if(!$results){
				echo "\nPDOStatement::errorInfo():\n";
				$arr = $st->errorInfo();
				print_r($arr);
			}
 		}
	}


}