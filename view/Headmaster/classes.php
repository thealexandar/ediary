<main role="main" class="ml-sm-auto px-4 main">
  <div class="pt-3 pb-2 mb-3 border-bottom text-center">
  <h1 class="h2">Classes</h1>
    </div>

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
   
   <div class="input-group-prepend d-block col-12 pl-0 pr-0">
       <a href="Headmaster/headmaster_dashboard" class="btn btn-info float-left" role="button">Back</a>
   </div>
 </div>

    <div class="col-12 border-bottom">
    <form action="" method="post">
      <div class="input-group mb-3 col-4">
        <div class="input-group-prepend">
          <label class="input-group-text" for="inputGroupSelect01">Select a Grade</label>
        </div>
            <select class="custom-select" id="inputGroupSelect01" name="value">
                <option selected>Choose...</option>
                <option value="1" class="font-weight-bold">1</option>
                <option value="2" class="font-weight-bold">2</option>
                <option value="3" class="font-weight-bold">3</option>
                <option value="4" class="font-weight-bold">4</option>
                <option value="5" class="font-weight-bold">5</option>
                <option value="6" class="font-weight-bold">6</option>
                <option value="7" class="font-weight-bold">7</option>
                <option value="8" class="font-weight-bold">8</option>
            </select>
            <div class="input-group-append">
            <button name="submit" type="submit" class="btn default-btn float-right">Confirm</button>
            </div>
        </form>
      </div>
    </div>
    <div class="col-12 pt-5">
    <?php 
    $filter = $data['class'];
    if(is_array($filter) || is_object($filter)) {
    foreach($filter as $flt): ?>
        <a href="Headmaster/classes_statistics&class=<?php echo $flt->id_cl; ?>">
            <div class="card m-0 float-left main-card animated classes_card custom-card mb-3">
              <div class="card-body text-center m-0">
                <span><i class="material-icons card-icon" style="color:#82ccdd;">school</i></span>
              </div>
              <div class="card-footer bg-transparent text-center">
                <h3><?php echo $flt->class; ?></h3>
              </div>
           </div>
        </a>
    <?php endforeach;
    }?>
    </div>

    </main>