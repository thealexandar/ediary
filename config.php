<?php
define("DB", "diary");
define("DBHOST", "localhost");
define("DBUSER", "root");
define("DBPASS", "");
define("APP_DIR","wamp64/www/e_diary");


function autoloadCore($class){
  require_once "core/{$class}.php";
}
spl_autoload_register("autoloadCore");

//ne radi:
// function autoloadModel($class){
// 	require_once "model/{$class}.php";
// }
// spl_autoload_register("autoloadModel");
