<?php

class MessageController extends Controller {

  private $id_user_login; 
  public function __construct() {
      $this->messageModel = $this->model('MessageModel');
      $this->$id_user_login = $_SESSION['user_id'];
    }

     // show all user
   public function show_user()
   {   
      $user_id_status = $_SESSION['user_status']; 
      
      if($user_id_status === 3) {
        $users = $this->messageModel->getParents();
      } else if ($user_id_status === 4) {
         $users = $this->messageModel->getTeachers();
      }
      
      foreach ($users as $key => $value)
      {
        array_push($arrayUser, [
          "user_id" => $users[$key]->id,
          "username" => $users[$key]->username
        ]);
      }

      echo (json_encode($arrayUser));
      return;
   }
   // show all message sender and reciver
   public function show_user_messages()
   {
     if($_SERVER['REQUEST_METHOD'] === "GET") {
       if(isset($_GET['user_id'])) {
         $user_id = $this->secure_input($_GET['user_id']);

         $messages = $this->MessageModel->showMessage($this->id_user_login, $user_id); 

         foreach ($messages as $key => $value)
         {
           array_push($arrayMessages, [
             "id_message" => $messages[$key]->id,
             "sender_id" => $messages[$key]->senderId,
             "reciver_id" => $messages[$key]->recieverId,
             "message" => $messages[$key]->body,
             "time" => $messages[$key]->time,
             "status" => $messages[$key]->status
           ]);
         }

         echo (json_encode($arrayMessages));
       }
     }
    return;
   }
    /// Live read new message
   public function read_message()
   {
    if($_SERVER['REQUEST_METHOD'] === "POST") {
      if(isset($_POST['user_id'])) {
         
        $messages = $this->messageModel->read_new_Message($sender, $recieverId);

         foreach ($messages as $key => $value)
         {
           array_push($arrayMessages, [
             "id_message" => $messages[$key]->id,
             "sender_id" => $messages[$key]->senderId,
             "reciver_id" => $messages[$key]->recieverId,
             "message" => $messages[$key]->body,
             "time" => $messages[$key]->time,
             "status" => $messages[$key]->status
           ]);

           $this->messageModel->new_status_Message($messages[$key]->id);
         }

         echo (json_encode($arrayMessages));

         return;
      }
    }
   }
   // Add new message
   public function new_message()
   {
     if($_SERVER['REQUEST_METHOD'] === "POST") {
        if(isset($_POST['user_id']) && isset($_POST['message'])) {
          $user_id = $this->secure_input($_POST['user_id']);
          $message = $this->secure_input($_POST['message']);


          $this->messageModel->addMessage($this->id_user_login,$user_id,$message);
        }
     }
     return;
   }

   private function secure_input($data)
   {
     $data = trim($data);
     $data = stripslashes($data);
     $data = htmlspecialchars($data);
     return $data;
   }

}