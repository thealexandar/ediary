<main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">

        <h2 class="h2">
        <?php
        $results = $data['timetable'];
        if(empty($results)) echo "";
        else { echo "Timetable for Class ". $results[0]->{'class'};} ?></h2>

    </div>

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <div class="input-group mb-3 col-12  pl-0 pr-0">
      <div class="input-group-prepend d-block col-12 pl-0 pr-0">
         <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
        Add New Timetable Insert +
        </button>
        <a href="Admin/timetable_admin" class="btn btn-info float-left" role="button">Back to Timetables</a>
      </div>
      </div>
    </div>

<div class="row">
 <div class="col-12">
  <div class="card-deck">
  <div class="card border-dark text-light mb-3" style="max-width: 20rem;">
  <div class="card-header bg-dark">Monday</div>
  <div class="card-body text-dark">
  <table class="table mb-0">
        <tbody>
        <?php
        $mon = $data['mon'];
        foreach($mon as $m) { ?>
        <tr>
            <td><?= $m->lesson;?></td>
            <td><?= $m->subject;?></td>
            <td><?= $m->tname . " " . $m->tsurname; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
  </div>
</div>
<div class="card border-dark text-light mb-3" style="max-width: 20rem;">
  <div class="card-header bg-dark">Tuesday</div>
  <div class="card-body text-dark">
  <table class="table">
        <tbody>
        <?php
        $tue = $data['tue'];
        foreach($tue as $t) { ?>
        <tr>
            <td><?= $t->lesson;?></td>
            <td><?= $t->subject;?></td>
            <td><?= $t->tname . " " . $t->tsurname; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
  </div>
</div>
<div class="card border-dark text-light mb-3" style="max-width: 20rem;">
  <div class="card-header bg-dark">Wednesday</div>
  <div class="card-body text-dark">
  <table class="table">
        <tbody>
        <?php
        $wed = $data['wed'];
        foreach($wed as $w) { ?>
        <tr>
            <td><?= $w->lesson;?></td>
            <td><?= $w->subject;?></td>
            <td><?= $w->tname . " " . $w->tsurname; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
  </div>
</div>
<div class="card border-dark text-light mb-3" style="max-width: 20rem;">
  <div class="card-header bg-dark">Thursday</div>
  <div class="card-body text-dark">
  <table class="table">
        <tbody>
        <?php
        $thu = $data['thu'];
        foreach($thu as $th) { ?>
        <tr>
            <td><?= $th->lesson;?></td>
            <td><?= $th->subject;?></td>
            <td><?= $th->tname . " " . $th->tsurname; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
  </div>
</div>
<div class="card border-dark text-light mb-3" style="max-width: 20rem;">
  <div class="card-header bg-dark">Friday</div>
  <div class="card-body text-dark">
  <table class="table">
        <tbody>
        <?php
        $fri = $data['fri'];
        foreach($fri as $f) { ?>
        <tr>
            <td><?= $f->lesson;?></td>
            <td><?= $f->subject;?></td>
            <td><?= $f->tname . " " . $f->tsurname; ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
  </div>
</div>
</div>
</div>
</div>


    <!-- The list of all the inserts -->
    <table class="table">
          <thead class="thead-custom">
              <th scope="col">ID</th>
              <th scope="col">Day</th>
              <th scope="col">Lesson No</th>
              <th scope="col">Subject</th>
              <th scope="col">Action</th>
              </tr>
          </thead>
          <tbody>
          <?php
           $perPage = 8;
           $page = isset($_GET['page']) ? intval($_GET['page']-1) : 0;
           $numberOfPages = intval(count($results)/$perPage)+1;
           foreach(array_slice($results, $page*$perPage, $perPage) as $result) { ?>

          <tr>
              <th scope="row"><?= $result->id_time;?></th>
              <td><?= $result->day;?></td>
               <td><?= $result->lesson;?></td>
                <td><?= $result->subject;?></td>
                <td>
                <form action="" method="post">
                    <div class="input-group mb-3">

                      <!-- <button type="button" class="btn btn-submitt" data-toggle="modal" data-target="#updateModal">
                         Update
                        </button> -->
                        <input type="hidden" name="id_time" value="<?= $result->id_time;?>">
                      <input type="submit" name="delete" value="Delete" id="delete" class="btn btn-danger">
                    </div>
                  </form>
                </td>
              </tr>
          <?php } ?>

          </tbody>
          </table>
          <nav aria-label="pagination">
            <ul class="pagination pg-blue justify-content-center">
             <?php
              for($i=1; $i<=$numberOfPages; $i++){?>
              <li class='page-item'><a class="page-link" href="Admin/timetable_add&class=<?= $results[0]->{'id_cl'};?>&page=<?=$i?>"><?= $i ?></a></li>
             <?php } ?>
            </ul>
          </nav>
    </main>

<!-- MODAL ADD -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="exampleModalLabel">Add New Timetable Input</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
        <form action="" method="post">

        <div class="form-group">
            <label for="exampleInputEmail1">Day</label>
            <select class="form-control" name="day">
            <?php 
            $day = $data['day'];
            foreach($day as $d) { ?>
            <option value="<?=$d->id_days; ?>"><?=$d->day; ?></option>
            <?php } ?>
          </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Lesson</label>
            <select class="form-control" name="lesson">
              <?php 
              $lessons = $data['lesson'];
              foreach($lessons as $l) { ?>
              <option value="<?=$l->id_l; ?>"><?=$l->lesson; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Subject</label>
            <select class="form-control" name="subj">
              <?php
               $subj=$data['subj'];
              foreach($subj as $s) { ?>
              <option value="<?=$s->id_subj; ?>"><?=$s->subject; ?></option>
              <?php } ?>
            </select>
          </div>

          <input type="submit" name="submit" value="Submit" class="btn btn-primary float-right" onclick="return message();">
        </form>
          </div>
      </div>
    </div>
  </div>

  <!-- MODAL UPDATE -->
  <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="exampleModalLabel">Update Timetable Input</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="" method="post">
          
          <div class="form-group">
            <label for="exampleInputEmail1">Id</label>
            <input name="id_time" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="<?=$result->id_time;?>" required>
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Day</label>
            <select class="form-control" name="day">
            <?php 
            $day = $data['day'];
            foreach($day as $d) { ?>
            <option value="<?=$d->id_days; ?>"><?=$d->day; ?></option>
            <?php } ?>
          </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Lesson</label>
            <select class="form-control" name="lesson">
              <?php 
              $lessons = $data['lesson'];
              foreach($lessons as $l) { ?>
              <option value="<?=$l->id_l; ?>"><?=$l->lesson; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Subject</label>
            <select class="form-control" name="subj">
              <?php
               $subj=$data['subj'];
              foreach($subj as $s) { ?>
              <option value="<?=$s->id_subj; ?>"><?=$s->subject; ?></option>
              <?php } ?>
            </select>
          </div>

          <div class="input-group mb-3">
            <input type="hidden">
            <input type="submit" value="Update" name="update" id="updatebtn" class="btn btn-success">
          </div>

          </form>
          </div>
      </div>
    </div>
  </div>


