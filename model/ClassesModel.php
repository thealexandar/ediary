<?php
   
class ClassesModel {

    private $conn;

	public function __construct() {
		$this->conn = new Database();
    }
    
    public function getAll(){
        $this->conn->query("SELECT * FROM classes");
            $results = $this->conn->resultset();
            return $results;
          } 

    public function getById($id) {
        $this->conn->query('SELECT * FROM classes WHERE id_cl = :id');
        $this->conn->bind(':id', $id);
        $result = $this->conn->single();
        return $result;
    }
          
    public function add() {
        if(isset($_POST['addNew'])){
            $year = $_POST['class_year'];
            $name = $_POST['class_name'];
            $full_name = $year . $name;
            $this->conn->query("INSERT INTO classes (class) VALUES (:class)");
            $this->conn->bind(':class', $full_name);
            if($this->conn->execute()) {
				// header("Location: users_admin");
				return true;
			} else {
				return false;
			}
        }
        
    }

    public function filter() {
        if(isset($_POST['submit'])) {
            $q = $_POST['value'];
            $this->conn->query("SELECT * FROM classes WHERE class LIKE '%$q%'");
            $results = $this->conn->resultset();
            if($results) {
				return $results;
				
			}else {
				return false;
			}
        }
    }

    public function delete() {
		if(isset($_POST['delete'])) {
			$id = $_POST['id_cl'];
			$this->conn ->query("delete from classes where id_cl = {$id}");
			$results = $this->conn->execute();
			if(!$results){
				echo "\nPDOStatement::errorInfo():\n";
				$arr = $st->errorInfo();
				print_r($arr);
	}
 }
}
}