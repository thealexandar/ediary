function message() {
  alert("Your record is successfully saved.");
  return true;
}

function request(id) {
  var id_user = id;
  document.getElementById(id).style.display = "none";

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    }
  };
  xmlhttp.open("GET", "Ajax/ajaxs&id=" + id_user, true);
  xmlhttp.send();
}

function accepted() {
  alert("You have accepted the request.");
  return true;
}

function declined() {
  alert("You have declined the request.");
  return true;
}

// $("body").on("click", "button", function(e) {
//   alert(e.target.id);
//   $(this).hide();
// });

var chat = $("#chat");
chat.scrollTop(chat.prop("scrollHeight"));

var $hamburger = $(".hamburger");
var $sidebar = $(".sidebar");
var $main = $(".main");
$hamburger.on("click", function(e) {
  $hamburger.toggleClass("is-active");
  $hamburger.toggleClass("hamburger-full");
  $sidebar.toggleClass("sidebar-sm");
  $main.toggleClass("main-full");
});
