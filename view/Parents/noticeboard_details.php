<main role="main" class="ml-sm-auto px-4 main">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
      <div class="input-group mb-3 col-12  pl-0 pr-0">
      <div class="input-group-prepend d-block col-12 pl-0 pr-0">
        <a href="Parents/noticeboard_parent" class="btn btn-info float-left" role="button">Back to Noticeboard</a>
      </div> 
    </div>
   
    </div>
    <div class="row">
      <div class="col-12">
        <div class="card mb-3 card-details col-8 ml-auto mr-auto pl-0 pr-0">
          <div class="card-header text-light">
            <?php $results=$data["notice"]; ?>
            <h3 class="card-title mb-0"><?php echo $results[0]->{'title'}; ?></h3>
          </div>
            <div class="card-body">
              <small class="mb-3 d-block"><?php echo $results[0]->{'date'}; ?></small>
              <hr>
              <p class="card-text"><?php echo $results[0]->{'message'}; ?></p>
            </div>
          </div>
      </div>
    </div>
</main>
