<main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
      <h1 class="h2">Send an Open Door Request</h1>
  </div>
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
   
   <div class="input-group-prepend d-block col-12 pl-0 pr-0">
       <a href="Parents/opendoor_parent" class="btn btn-info float-left" role="button">Back to Opendoor Section</a>
   </div>
 </div>

  <table class="table">
      <thead class="thead-dark">
          <tr>
          <th scope="col">Teacher</th>
          <th scope="col">Day</th>
          <th scope="col">Time</th>
          <th scope="col">Action</th>
          </tr>
      </thead>
      <tbody>
      <?php 
      $results = $data['opendoor'];
      foreach($results as $result): ?>
      <tr>
        <td><?=$result->tname . " " . $result->tsurname;?></td>
        <td><?= $result->day;?></td>
        <td><?= $result->hour . " : " . $result->min . " " . $result->period;?></td>
        <td>
        <form action="" method="post" >
          <div class="input-group-append">
          <input type="hidden" value="<?= $result->id_tos;?>">
          <button onclick="request(<?= $result->id_tos; ?>)" type="button" id="<?= $result->id_tos; ?>" value = "<?= $result->id_tos;?>" class="btn btn-primary button float-right">Send Request</button>
          </div>
        </form>
        </td>
      </tr>
      <?php endforeach; ?>
      </tbody>
  </table>
</main>
