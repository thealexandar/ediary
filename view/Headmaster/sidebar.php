<div class=" text-center sidebar">
        <div class="avatar ml-auto mr-auto mb-3">
            <i class="material-icons avatar-icon">
                person
            </i>
        </div>
      <?php if(isset($_SESSION['user_id'])) : ?>
            <h1 class="text-light ml-auto mb-0"><?php echo $_SESSION['user_username']; ?></h1>
            <ul class="nav flex-column mb-5">
                <li class="nav-item logout">
                <a class="nav-link pl-0 pr-0" href="Users/logout">Sign out</a>
                </li>
            </ul>
        <?php endif; ?>
        <hr class="sidebar-hr">
        <ul class="nav flex-column text-left ml-auto mr-auto sidebar-nav">
            <li class="nav-item">
                <a class="nav-link active" href="Headmaster/headmaster_dashboard">
                <span><i class="fas fa-chart-line sidebar-icon"></i></span>
                Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item hvr-underline-from-center">
                <a class="nav-link" href="Headmaster/classes">
                <span><i class="material-icons sidebar-icon">school</i></span>
                Statistics By Classes
                </a>
            </li>

           <li class="nav-item">
                <a class="nav-link" href="Headmaster/school_statistics">
                    <span><i class="material-icons sidebar-icon">info</i></span>
                School Statistics
                </a>
            </li>


        </ul>
      </div>