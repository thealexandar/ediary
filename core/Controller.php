<?php
//namespace App\Core;
abstract class Controller {
  public function loadView($view, $data=[], $params=array()) {
		foreach ($params as $k=>$param) {
      $this->$k = $param;
		}
    include "view/{$view}.php";
  }

  public function index() {
    echo "Podrazumevana strana";
	}
  
// Load model
public function model($model){
  // Require model file
  require_once 'model/' . $model . '.php';

  // Instatiate model
  return new $model();
  }
}
?>