<main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
        <h1 class="h2">Noticeboard</h1>
        <button type="button" class="btn success-btn" data-toggle="modal" data-target="#noticeboard">
          Add New +
        </button>
    </div>
    <div class="row">
      <div class="col-12">
      <?php 
       $perPage = 8;
       $page = isset($_GET['page']) ? intval($_GET['page']-1) : 0;
       $results = $data['notice'];
       $numberOfPages = intval(count($results)/$perPage)+1;
       foreach(array_slice($results, $page*$perPage, $perPage) as $result) { ?>
        <div class="card custom-card mb-3">
          <div class="card-body">
            <h5 class="card-title"><?php echo $result->title ?></h5>
            <small class="mb-3 d-block"><?php echo $result->date ?></small>
            <p class="card-text text-truncate"><?php echo $result->message ?></p>
            <a href="Admin/noticeboard_details&id=<?php echo $result->id_news; ?>" class="btn confirm-btn default-btn btn-wide">More</a>
          </div>
          <form action="" method="post">
              <div class="input-group mb-3">
              <input type="hidden" name="id_news" value="<?= $result->id_news;?>">
              <input type="submit" name="delete" value="Delete" id="delete" class="btn btn-danger btn-sm ml-auto">
            </div>
          </form>
        </div>
       <?php } ?>
      </div>
    </div>
    <nav aria-label="pagination">
        <ul class="pagination pg-blue justify-content-center">
          <?php 
          for($i=1; $i<=$numberOfPages; $i++){?>
          <li class='page-item'><a class="page-link" href="Admin/noticeboard_admin&page=<?=$i?>"><?= $i ?></a></li>
          <?php } ?>
        </ul>
      </nav>
</main>

<!-- ADD NEW NOTICE MODAL -->
<div class="modal fade" id="noticeboard" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="noticeboar">Add Notice</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Notice Name</label>
            <input name="title" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Notice Name" required>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Notice Content</label>
            <textarea name="message" class="form-control z-depth-1" id="exampleFormControlTextarea6" rows="7" placeholder="Write notice here..."></textarea>
          </div>
            <input type="submit" name="submit" value="Submit" class="btn confirm-btn float-right" onclick="return message();">
          </form>
          </div>
          </div>
    </div>
  </div>


