<?php

	class NoticeboardModel {
			private $conn;

			public function __construct() {
				$this->conn = new Database();
			}

		public function add(){
			if(isset($_POST['submit'])){
				$this->conn->query("INSERT INTO news (title, message) VALUES (:title, :message)");
				$this->conn->bind(':title', $_POST['title']);
				$this->conn->bind(':message', $_POST['message']);
				if($this->conn->execute()) {
					header("Location:noticeboard_admin");
					return true;
				} else {
					return false;
				}
			}

		}
		public function getAll() {
			$this->conn->query("SELECT * FROM news ORDER BY date desc");
			$results = $this->conn->resultset();
			return $results;
		}

		public function getById($filter) {
			$this->conn->query("SELECT * FROM news WHERE id_news =" . $filter);
			$results = $this->conn->resultset();
			return $results;
		}

		public function delete() {
			if(isset($_POST['delete'])) {
				$id = $_POST['id_news'];
				$this->conn ->query("DELETE FROM news WHERE id_news = {$id}");
				$results = $this->conn->execute();
				if(!$results){
					echo "\nPDOStatement::errorInfo():\n";
					$arr = $st->errorInfo();
					print_r($arr);
		}
	 }
	}
}