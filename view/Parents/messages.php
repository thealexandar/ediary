<main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
      <h1 class="h2 text-light">Messages</h1>

    </div>
        <div class="row">
            <div class="col-md-9 inbox pr-0">
                <div class="chatbox pl-3" id="chat">
                <p>

                </p>

                </div>
                <div class="typemessage mt-0">
                    <form action="" method="post" class="row mr-0 ml-0">
                        <input type="text" name="message" class="form-control col-10 d-inline" placeholder="Type your message here..." autofocus="autofocus">
                        <input type="submit" name="submit" class="btn btn-chat col-2">
                    </form>
                </div>
            </div>
            <div class="col-md-3 users-list pl-0 pr-0">
                <ul class="nav flex-column text-center">
                <?php 
                    $teachers = $data['msg'];
                    foreach($teachers as $teacher): ?>
                    <li class="nav-item">
                        <a class="nav-link text-light p-4" href="Parents/show?id=<?= $teacher->id; ?>"><?= $teacher->tname . " ". $teacher->tsurname; ?></a>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
</main>