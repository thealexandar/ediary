<main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
        <h1 class="h2">Choose a class</h1>
    </div>

    <div class="col-12 pt-5">
    <?php 
    $classes = $data['class'];
    foreach($classes as $class): ?>
        <a href="Teacher/classes&class=<?php echo $class->id_cl; ?>">
        <div class="card m-0 float-left main-card animated classes_card custom-card mb-3">
            <div class="card-body text-center m-0">
                <span><i class="material-icons card-icon" style="color:#82ccdd;">school</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3><?php echo $class->class; ?></h3>
            </div>
        </div>
        </a>
    <?php endforeach;
    ?>
    </div>

    </main>
