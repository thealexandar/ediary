<main role="main" class="ml-sm-auto px-4 main">
  <div class="pt-3 pb-2 mb-3 border-bottom text-center">
        <h1>Welcome</h1>

    </div>

    <div class="col-12 pt-5">

        <a href=Headmaster/classes>
            <div class="card m-0 float-left main-card animated bounceIn bounce-delay_first classes_card">
                <div class="card-body text-center m-0">
                    <span><i class="material-icons card-icon" style="color:#ffeaa7;">school</i></span>
                </div>
                <div class="card-footer bg-transparent text-center">
                    <h3>Statistics By Classes</h3>
                </div>
            </div>
        </a>
        <a href=Headmaster/school_statistics>
            <div class="card m-0 float-left main-card animated bounceIn bounce-delay_second classes_card">
                <div class="card-body text-center m-0">
                    <span><i class="material-icons card-icon" style="color:#25CCF7;">open_in_browser</i></span>
                </div>
                <div class="card-footer bg-transparent text-center">
                    <h3>School Statistics By Subjects</h3>
                </div>
            </div>
        </a>



    </div>

    </main>