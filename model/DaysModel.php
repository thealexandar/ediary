<?php
  
class DaysModel {

  private $conn;

  public function __construct() {
    $this->conn = new Database();
    }
    
  public function getAll(){
    $this->conn->query("SELECT * FROM days");
      $results = $this->conn->resultset();
      return $results;
    } 
}