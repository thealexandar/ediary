<main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
        <h1 class="h2">Noticeboard</h1>

    </div>
    <div class="row">
      <div class="col-12">
      <?php 
      $perPage = 8;
      $page = isset($_GET['page']) ? intval($_GET['page']-1) : 0;
      $results = $data['notice'];
      $numberOfPages = intval(count($results)/$perPage)+1;
      foreach(array_slice($results, $page*$perPage, $perPage) as $result) { ?>
        <div class="card custom-card mb-3">
          <div class="card-body">
            <h5 class="card-title"><?= $result->title ?></h5>
            <small class="mb-3 d-block"><?=  $result->date ?></small>
            <p class="card-text text-truncate"><?= $result->message ?></p>
            <a href="Parents/noticeboard_details&id=<?= $result->id_news; ?>" class="btn confirm-btn btn-wide">More</a>
          </div>
        </div>
      <?php } ?>
      </div>
    </div>
</main>
