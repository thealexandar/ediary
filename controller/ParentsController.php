<?php

class ParentsController extends Controller {

  public function __construct() {
    $this->userModel = $this->model('UserModel');
    $this->subjectModel = $this->model('SubjectModel');
    $this->classesModel = $this->model('ClassesModel');
    $this->teacherModel = $this->model('TeacherModel');
    $this->teacherSubjectClassModel = $this->model('TeacherSubjectClassModel');
    $this->studentModel = $this->model('StudentModel');
    $this->timetableModel = $this->model('TimetableModel');
    $this->noticeboardModel = $this->model('NoticeboardModel');
    $this->daysModel = $this->model('DaysModel');
    $this->lessonsModel = $this->model('LessonsModel');
    $this->messageModel = $this->model('MessageModel');
    $this->opendoorModel = $this->model('OpendoorModel');
    $this->gradesModel = $this->model('GradesModel');

    if(!isset($_SESSION['user_id'])) {
      header("Location: ../Users/login");
    }
  }

  function index() {
    $this->loadView("header");
    $this->loadView("Parents/sidebar");
    $this->loadView("Parents/dashboard_parent");
    $this->loadView("footer");
  }

  function messages() {
    $sender = $_SESSION['user_id'];
    $get = $this->messageModel->getTeachers();
    $data = [
      'msg' => $get
    ];
    $this->loadView("header");
    $this->loadView("Parents/sidebar");
    $this->loadView("Parents/messages", $data);
    $this->loadView("footer");
  }

  function show() {
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    $parts = parse_url($actual_link);
    parse_str($parts['query'], $query);
    $show = $this->messageModel->showMessage($_SESSION['user_id'], $query['id']);
    $get = $this->messageModel->getTeachers();
    $add = $this->messageModel->addMessage($_SESSION['user_id'], $query['id'], 'message');
    //ovo message promeni
    $data = [
      'msg' => $show,
      'teachers' => $get,
      'add' => $add
    ];
    $this->loadView("header");
    $this->loadView("Parents/sidebar");
    $this->loadView("Parents/show", $data);
    $this->loadView("footer");
  }

  function subjects_parent() {
    $get = $this->gradesModel->getById("st.id =".$_SESSION['user_id']);
    $data = [
      'grade' => $get
    ];
    $this->loadView("header");
    $this->loadView("Parents/sidebar");
    $this->loadView("Parents/subjects_parent", $data);
    $this->loadView("footer");
  }
  
  function opendoor_parent() {
    $this->loadView("header");
    $this->loadView("Parents/sidebar");
    $this->loadView("Parents/opendoor_parent");
    $this->loadView("footer");
  }

  function opendoor_timeslots() {
    $get = $this->opendoorModel->getSlotsForParents($_SESSION['user_id']);
    $data = [
      'opendoor' => $get
    ];
    $this->loadView("header");
    $this->loadView("Parents/sidebar");
    $this->loadView("Parents/opendoor_timeslots", $data);
    $this->loadView("footer");
  }

  function opendoor_responses() {
    $get = $this->opendoorModel->getResponsesForParents($_SESSION['user_id']);
    $remove = $this->opendoorModel->remove_response();
    $data = [
      'opendoor' => $get
    ];
    $this->loadView("header");
    $this->loadView("Parents/sidebar");
    $this->loadView("Parents/opendoor_responses", $data);
    $this->loadView("footer");
  }

  function noticeboard_parent() {
    $get = $this->noticeboardModel->getAll();
    $data = [
      'notice' => $get
    ];
    $this->loadView("header");
    $this->loadView("Parents/sidebar");
    $this->loadView("Parents/noticeboard_parent", $data);
    $this->loadView("footer");
  }

  function noticeboard_details() {
    $get = $this->noticeboardModel->getById($_GET['id']);
    $data = [
      'notice' => $get
    ];
     $this->loadView("header");
     $this->loadView("Parents/sidebar");
     $this->loadView("Parents/noticeboard_details", $data);
     $this->loadView("footer");
   }

   function timetable() {
    $get = $this->timetableModel->getForStudents("");
    $mon = $this->timetableModel->getForStudents(" AND d.id_days=1");
    $tue = $this->timetableModel->getForStudents(" AND d.id_days=2");
    $wed = $this->timetableModel->getForStudents(" AND d.id_days=3");
    $thu = $this->timetableModel->getForStudents(" AND d.id_days=4");
    $fri = $this->timetableModel->getForStudents(" AND d.id_days=5");
    $data = [
      'timetable' => $get,
      'mon' => $mon,
      'tue' => $tue,
      'wed' => $wed,
      'thu' => $thu,
      'fri' => $fri
    ];
    $this->loadView("header");
    $this->loadView("Parents/sidebar");
    $this->loadView("Parents/timetable", $data);
    $this->loadView("footer");
  }
  
}