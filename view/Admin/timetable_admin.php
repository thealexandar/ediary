<main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
      <h1 class="h2">Timetables</h1>
      <div class="input-group mb-3 col-4">
      <div class="input-group-prepend">
        
      </div>
    </div>  
  </div>

<div class="col-12 border-bottom">
  <form action="" method="post">
    <div class="input-group mb-3 col-4">
      <div class="input-group-prepend">
        <label class="input-group-text" for="inputGroupSelect01">Select a Grade</label>
      </div>
      
          <select class="custom-select" id="inputGroupSelect01" name="value">
              <option selected>Choose...</option>

              <option value="1" class="font-weight-bold">1</option>
              <option value="2" class="font-weight-bold">2</option>
              <option value="3" class="font-weight-bold">3</option>
              <option value="4" class="font-weight-bold">4</option>
              <option value="5" class="font-weight-bold">5</option>
              <option value="6" class="font-weight-bold">6</option>
              <option value="7" class="font-weight-bold">7</option>
              <option value="8" class="font-weight-bold">8</option>

          </select>
          <div class="input-group-append">
          <button name="submit" type="submit" class="btn btn-success float-right">Confirm</button>
              
          </div>
      </form>

    </div>
  </div>
  <div class="col-12 pt-5">
  <?php 
  $filter = $data["filter"];
  if(is_array($filter) || is_object($filter)) {
  foreach($filter as $flt): ?>
      <a href="Admin/timetable_add&class=<?php echo $flt->id_cl; ?>">
      <div class="card m-0 float-left main-card animated classes_card custom-card mb-3">
              <div class="card-body text-center m-0">
              <span><i class="material-icons card-icon style="color:#b8e994;">schedule</i></span>
              </div>
              <div class="card-footer bg-transparent text-center">
                  <h3><?php echo $flt->class; ?></h3>
              </div>
          </div>
      </a>
  <?php endforeach; 
  }?>
  </div>
</main>



