<?php

class HeadmasterController extends Controller {
  
  public function __construct() {
    $this->classesModel = $this->model('ClassesModel');
    $this->gradesModel = $this->model('GradesModel');

    if(!isset($_SESSION['user_id'])) {
      header("Location: ../Users/login");
    }
  }
  
  function index() {
    $this->loadView("header");
    $this->loadView("Headmaster/sidebar");
    $this->loadView("Headmaster/headmaster_dashboard");
    $this->loadView("footer");
  }
  function classes_statistics() {
    $get = $this->gradesModel->getByClass($_GET["class"]);
    $data = [
      'gr' => $get
    ];
    $this->loadView("header");
    $this->loadView("Headmaster/sidebar");
    $this->loadView("Headmaster/classes_statistics", $data);
    $this->loadView("footer");
  }
  function school_statistics() {
    $get = $this->gradesModel->getBySchool();
    $data = [
      'grades' => $get
    ];
    $this->loadView("header");
    $this->loadView("Headmaster/sidebar");
    $this->loadView("Headmaster/school_statistics", $data);
    $this->loadView("footer");
  }
  function classes() {
    $filter = $this->classesModel->filter();
    $data = [
      'class' => $filter
    ];
    $this->loadView("header");
    $this->loadView("Headmaster/sidebar");
    $this->loadView("Headmaster/classes", $data);
    $this->loadView("footer");
  }
}