<main role="main" class="ml-sm-auto px-4 main">
    <div class="col-12 pt-5">

    <a href=Parents/subjects_parent>
        <div class="card m-0 float-left main-card animated bounceIn bounce-delay_first classes_card">
            <div class="card-body text-center m-0">
                <span><i class="material-icons card-icon" style="color:#82ccdd;">school</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Grades</h3>
            </div>
        </div>
    </a>
    <a href=Parents/timetable>
        <div class="card m-0 float-left main-card animated bounceIn bounce-delay_second classes_card">
            <div class="card-body text-center m-0">
                <span><i class="material-icons card-icon" style="color:#b8e994;">schedule</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Timetable</h3>
            </div>
        </div>
    </a>
    <a href=Parents/messages>
        <div class="card m-0 float-left main-card animated bounceIn bounce-delay_third classes_card">
            <div class="card-body text-center m-0">
                <span><i class="material-icons card-icon" style="color:#25CCF7;">forum</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Messages</h3>
            </div>
        </div>
    </a>
    <a href=Parents/opendoor_parent>
        <div class="card m-0 float-left main-card animated bounceIn bounce-delay_fourth classes_card">
            <div class="card-body text-center m-0">
                <span><i class="material-icons card-icon" style="color:#ffeaa7;">people</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Open Door</h3>
            </div>
        </div>
    </a>
    <a href=Parents/noticeboard_parent>
        <div class="card m-0 float-left main-card animated bounceIn bounce-delay_fifth classes_card">
            <div class="card-body text-center m-0">
                <span><i class="material-icons card-icon" style="color:#FEA47F;">info</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Noticeboard</h3>
            </div>
        </div>
    </a>
    </div>
</main>