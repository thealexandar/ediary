<?php
    $teacher = new Teacher();
    $teacher = $this->teacher;
  ?>

    <main role="main" class="ml-sm-auto px-4 main">
    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
        <h1 class="h2">Users</h1>
    </div>
      <table class="table">
          <thead class="thead-dark">
              <tr>
              <th scope="col">ID</th>
              <th scope="col">First Name</th>
              <th scope="col">Last Name</th>
              <th scope="col">Username</th>
              <th scope="col">Subject</th>
              <th scope="col">Class</th>
              <th scope="col">Action</th>
              </tr>
          </thead>
          <tbody>
              <tr>
              <form action="" method="post">
                <th scope="row"><?php ?></th>
                <td>
                  <div class="form-group">
                    <input name="id" type="text" class="form-control pl-1" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="<?= $teacher->id_teachers; ?>">
                  </div>
                </td>
                <td>
                  <div class="form-group">
                    <input name="name" type="text" class="form-control pl-1" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="<?= $teacher->name; ?>">
                  </div>
                </td>
                <td>
                  <div class="form-group">
                    <input name="surname" type="text" class="form-control pl-1" id="exampleInputPassword1" placeholder="<?=$teacher->surname; ?>">
                  </div>
                </td>
                <td>
                  <div class="form-group">
                    <input name="username" type="text" class="form-control pl-1" id="exampleInputPassword1" placeholder="<?=$teacher->username; ?>">
                  </div>
                </td>
                <td>
                  <div class="form-group">
                    <input name="pass" type="text" class="form-control pl-1" id="exampleInputPassword1" placeholder="<?=$teacher->password; ?>">
                  </div>
                </td>
                <td>
                 <div class="form-group">
            <label for="exampleInputPassword1">Status</label>

            <select name="status">
              <?php
                $statuses = Status::getAll();
                foreach($statuses as $status) { ?>
                  <option value="<?php echo $status->id_status; ?>"><?php echo $status->role; ?></option>
              <?php } ?>
            </select>
          </div>
                </td>
                <td>
                  <div class="input-group mb-3">
                    <input type="hidden" value="<?=$teacher->id_teachers; ?>">
                    <input type="submit" value="Submit" name="update" id="updatebtn" class="btn btn-success">
                  </div>
                </td>
              </form>
                <!-- <td>Matematika</td> -->
                <!-- <td>I-a</td> -->
              </tr>
          </tbody>
          </table>
    </main>

