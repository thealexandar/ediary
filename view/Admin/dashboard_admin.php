<main role="main" class="ml-sm-auto px-4 main">
    
    <div class="col-12 pt-5">
    
    <a href=Admin/users_admin>
    <div class="card m-0 float-left main-card animated bounceIn bounce-delay_first classes_card">
                <div class="card-body text-center m-0">
                    <span><i class="material-icons card-icon" style="color:#52458f;">group</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Users</h3>
            </div>
        </div>
    </a>
    <a href=Admin/subjects_admin>
    <div class="card m-0 float-left main-card animated bounceIn bounce-delay_second classes_card">
                <div class="card-body text-center m-0">
                    <span><i class="material-icons card-icon" style="color:#82ccdd;">school</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Subjects</h3>
            </div>
        </div>
    </a>
    <a href=Admin/teachers_admin>
    <div class="card m-0 float-left main-card animated bounceIn bounce-delay_third classes_card">
                <div class="card-body text-center m-0">
                    <span><i class="material-icons card-icon" style="color:#ffeaa7;">person</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Teachers</h3>
            </div>
        </div>
    </a>
    <a href=Admin/classes_admin>
    <div class="card m-0 float-left main-card animated  bounceIn bounce-delay_fourth classes_card">
                <div class="card-body text-center m-0">
                    <span><i class="material-icons card-icon" style="color:#238566;">format_list_bulleted</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3> Students-Classes</h3>
            </div>
        </div>
    </a>
    <a href="Admin/timetable_admin">
    <div class="card m-0 float-left main-card animated bounceIn bounce-delay_fifth classes_card">
                <div class="card-body text-center m-0">
                    <span><i class="material-icons card-icon style="color:#b8e994;">schedule</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Timetable</h3>
            </div>
        </div>
    </a>
    <a href="Admin/noticeboard_admin">
    <div class="card m-0 float-left main-card animated bounceIn bounce-delay_sixt classes_card">
                <div class="card-body text-center m-0">
                    <span><i class="material-icons card-icon" style="color:#FEA47F;">info</i></span>
            </div>
            <div class="card-footer bg-transparent text-center">
                <h3>Noticeboard</h3>
            </div>
        </div>
    </a>
</div>

</main>