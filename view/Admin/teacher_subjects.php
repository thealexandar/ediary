<main role="main" class="ml-sm-auto px-4 main">
  <div class="pt-3 pb-2 mb-3 border-bottom text-center">
    <h2 class="h2"> 
    <?php 
    $results=$data['get'];
    if(empty($results))
    echo "";
    else { 
    echo "Teacher: ". $results[0]->{'tname'} . " " . $results[0]->{'tsurname'} ;
    }
    ?></h2>
    </div>

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <div class="input-group mb-3 col-12  pl-0 pr-0">
      <div class="input-group-prepend d-block col-12 pl-0 pr-0">
         <!-- Button trigger modal -->
         <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
            Add Subject +
          </button>
          <a href="Admin/teachers_admin" class="btn btn-info float-left" role="button">Back to Teachers</a>
      </div>
      </div>
    </div>
      
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">Subject</th>
            <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($results as $result): ?>
            <tr>
              <td><?= $result->subject;?></td>
              <td>
                <form action="" method="post">
                  <div class="input-group mb-3">
                      <input type="hidden" name="id" value="<?= $result->id_ts;?>">
                    <input type="submit" name="delete" value="Delete" id="delete" class="btn btn-danger">
                  </div>
                </form>
              </td>
          </tr>
          <?php endforeach; ?>
        
        </tbody>
      </table>
</main>


<!-- Modal ADD -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="exampleModalLabel">Add</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <form action="" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Subject</label>
            <select name="subject">
              <?php
               $subjects=$data['subject'];
              foreach($subjects as $subj) { ?>
              <option value="<?=$subj->id_subj; ?>"><?=$subj->subject; ?></option>
              <?php } ?>
            </select>
          </div>

          <input type="submit" name="submit" value="Submit" class="btn btn-primary float-right" onclick="return message();">
        </form>
          </div>

      </div>
    </div>
  </div>


