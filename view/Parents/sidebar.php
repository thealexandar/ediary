<div class=" text-center sidebar">
<div class="avatar ml-auto mr-auto mb-3">
    <i class="material-icons avatar-icon">
        person
    </i>
</div>

<?php if(isset($_SESSION['user_id'])) : ?>
    <h3 class="text-light ml-auto mb-0"><?php echo $_SESSION['user_username']; ?></h3>
    <ul class="nav flex-column mb-3">
        <li class="nav-item logout">
        <a class="nav-link pl-0 pr-0" href="Users/logout">Sign out</a>
        </li>
    </ul>
<?php endif; ?>
<hr class="sidebar-hr">
<ul class="nav flex-column text-left ml-auto mr-auto sidebar-nav">
    <li class="nav-item">
        <a class="nav-link active" href="Parents/dashboard_parent">
        <span><i class="fas fa-chart-line sidebar-icon"></i></span>
        Dashboard <span class="sr-only">(current)</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="Parents/subjects_parent">
        <span><i class="material-icons sidebar-icon">school</i></span>
        Grades
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="Parents/timetable">
            <span><i class="material-icons sidebar-icon">schedule</i></span>
        Timetable
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="Parents/messages">
        <span><i class="material-icons sidebar-icon">forum</i></span>
        Messages
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="Parents/opendoor_parent">
        <span><i class="material-icons sidebar-icon">people</i></span>
        Open Door
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="Parents/noticeboard_parent">
            <span><i class="material-icons sidebar-icon">info</i></span>
        Noticeboard
        </a>
    </li>
</ul>
</div>