<?php
class StudentModel {


	private $conn;

	public function __construct() {
		$this->conn = new Database();
	}

public function getByClass($filter) {
		$this->conn->query("SELECT 
              *
							FROM 
							students 
							JOIN classes ON students.class = classes.id_cl
							WHERE classes.id_cl =" . $filter
							);
		$results = $this->conn->resultset();
		return $results;
  }

public function update($id) {
  if(isset($_POST['update'])) {
    $id = $_POST['id'];
    $this->conn->query('UPDATE students
    SET name=:name, surname=:surname, jmbg=:jmbg, pname=:pname, psurname=:psurname, username=:username, password=:password WHERE id=:id');
    $this->conn->bind(':id', $_POST['id']);
    $this->conn->bind(':name', $_POST['name']);
    $this->conn->bind(':surname', $_POST['surname']);
    $this->conn->bind(':jmbg', $_POST['jmbg']);
    $this->conn->bind(':pname', $_POST['pname']);
		$this->conn->bind(':psurname', $_POST['psurname']);
		$this->conn->bind(':username', $_POST['username']);
		$this->conn->bind(':password', $_POST['password']);
    if($this->conn->execute()) {
      return true;
    } else {
      return false;
    }
}
}

	
public function add($filter) {
	if(isset($_POST['submit'])){

		$this->conn->query('INSERT INTO students 
		SET name=:name, surname=:surname, jmbg=:jmbg, pname=:pname, psurname=:psurname, username=:username, password=:password, class=' .$filter. ', status=(SELECT id_status FROM status WHERE id_status=4)');

		$this->conn->bind(':name', $_POST['name']);
		$this->conn->bind(':surname', $_POST['surname']);
		$this->conn->bind(':jmbg', $_POST['jmbg']);
		$this->conn->bind(':pname', $_POST['pname']);
		$this->conn->bind(':psurname', $_POST['psurname']);
		$this->conn->bind(':username', $_POST['username']);
		$this->conn->bind(':password', $_POST['password']);

		if($this->conn->execute()) {
			header("Location:class_details&class=$filter");
			exit;
		} else {
			return false;
		}
	}
}

  public function delete() {
		if(isset($_POST['delete'])) {
			$id = $_POST['id'];
			$this->conn ->query("delete from students where id = {$id}");
			$results = $this->conn->execute();
			if(!$results){
				echo "\nPDOStatement::errorInfo():\n";
				$arr = $st->errorInfo();
				print_r($arr);
	}
 }
}


}