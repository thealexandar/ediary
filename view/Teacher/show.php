<main role="main" class="ml-sm-auto px-4 main">
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 border-bottom">
      <h1 class="h2 text-light"></h1>

</div>
    <div class="row">
        <div class="col-9 inbox pr-0">

            <div class="chatbox pl-3" id="chat">
            <p>
            <?php 
            $res = $data['msg'];
            foreach($res as $msg): ?>
                <p class="speech-bubble text-left text-light <?php echo ($msg->senderId == $_SESSION['user_id']) ? "sender text-left" : "reciever text-right"; ?> " id="textnode"><?php echo $msg->body; ?></p>
            <?php endforeach; ?>
            </p>

            </div>
            <div class="typemessage mt-0">
                <form action="" method="post" class="row mr-0 ml-0">
                    <input type="text" name="message" class="form-control col-10 d-inline" placeholder="Type your message here..." autofocus="autofocus">
                    <input type="submit" name="submit" class="btn btn-chat col-2">
                </form>
            </div>

        </div>
        <div class="col-md-3 users-list pl-0 pr-0">
            <ul class="nav flex-column text-center">
            <?php 
            $parents = $data['parents'];
            foreach($parents as $parent): ?>
                <li class="nav-item">
                    <a class="nav-link text-light p-4" href="Teacher/show?id=<?php echo $parent->id; ?>"><?php echo $parent->pname. " " .$parent->psurname; ?></a>
                </li>
            <?php endforeach; ?>
            </ul>
        </div>
    </div>

</main>