<?php

class SubjectModel{

	private $conn;
	public function __construct() {
		$this->conn = new Database();
	}

	public function getAll() {
		$this->conn->query("SELECT * FROM subjects");
		$results = $this->conn->resultset();
		return $results;
	}

	public function getSubjectsByClass($filter) {
		$this->conn->query("SELECT s.id_subj, s.subject
		FROM 
		subjects s
        JOIN teacher_subject ts ON s.id_subj = ts.id_subject
				JOIN teachers t ON ts.id_teacher = t.id
				JOIN teacher_class tc ON t.id = tc.id_teacher
				JOIN classes c ON tc.id_class = c.id_cl
				WHERE c.id_cl =" . $filter
		);
		$results = $this->conn->resultset();
		return $results;
	}

	public function add(){
		if(isset($_POST['submit'])){
		$this->conn->query("INSERT INTO subjects (subject)
		VALUES (:subject)");
		
		$this->conn->bind(':subject', $_POST['subject']);

		if($this->conn->execute()) {
			header("Location:subjects_admin");
			exit;
		} else {
			return false;
		}
	}	
}


	public function update(){
		if(isset($_POST['update'])) {
		$this->conn->query('UPDATE subjects
		SET subject = :subject
		WHERE subjects.id_subj=:id;');

		$this->conn->bind(':id', $_POST['id_subj']);
		$this->conn->bind(':subject', $_POST['subject']);
		if($this->conn->execute()) {
			return true;
		} else {
			return false;
		}
	}
}


	public function delete() {
		if(isset($_POST['delete'])) {
			$id = $_POST['id_subj'];
			$this->conn ->query("delete from  subjects where id_subj = {$id}");
			$results = $this->conn->execute();
			if(!$results){
				echo "\nPDOStatement::errorInfo():\n";
				$arr = $st->errorInfo();
				print_r($arr);	
	}
 }	
}
}