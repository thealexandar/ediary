
   <main role="main" class="ml-sm-auto px-4 main">

   <div class="pt-3 pb-2 mb-3 border-bottom text-center">
        <h1 class="h2">Timetable</h1>

         <div class="col-md-7">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
      Select New +
    </button>
  </div>
  <!-- Button trigger modal -->

    </div>
      <table class="table timetable">
          <thead class="thead-dark">
              <th scope="col mr-auto">Class</th>
              <th scope="col">Monday</th>
              <th scope="col">Tuesday</th>
              <th scope="col">Wednesday</th>
              <th scope="col">Thursday</th>
              <th scope="col">Friday</th>
              </tr>
          </thead>
          <tbody>
            <tr>
              <td class="border-right">1</td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
            </tr>
            <tr>
              <td class="border-right">2</td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
            </tr>
            <tr>
              <td class="border-right">3</td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
            </tr>
            <tr>
              <td class="border-right">4</td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
            </tr>
            <tr>
              <td class="border-right">5</td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
              <td class="border-right">
                <p class="mb-0 font-weight-bold text-center">Matematika</p>
                <small class="float-right">Bojana Plemic</small>
              </td>
            </tr>
          </tbody>
          </table>
    </main>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="modal-title" id="exampleModalLabel">Add New Class</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" method="post">
            <div class="form-group">
                <label for="inputState">Class Year</label>
                <select id="inputState" class="form-control" name="class_year" required>
                  <option selected>Choose...</option>
                  <option value="1">I</option>
                  <option value="2">II</option>
                  <option value="3">III</option>
                  <option value="4">IV</option>
                  <option value="5">V</option>
                  <option value="6">VI</option>
                  <option value="7">VII</option>
                  <option value="8">VIII</option>
                </select>
              </div>
              <div class="form-group">
                <label for="inputState">Class Name</label>
                <select id="inputState" class="form-control" name="class_name" required>
                  <option selected>Choose...</option>
                  <option value="a">a</option>
                  <option value="b">b</option>
                  <option value="c">c</option>
                  <option value="d">d</option>
                  <option value="e">e</option>
                  <option value="f">f</option>
                  <option value="g">g</option>
                  <option value="h">h</option>
                </select>
              </div>
              <button name="addNew" type="submit" class="btn btn-primary float-right">Submit</button>
            </form>
          </div>
      </div>
    </div>
  </div>

